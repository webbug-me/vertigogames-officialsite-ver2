# Vertigo Game Official Site Ver2
[https://vertigogames.com](https://vertigogames.com)
> Vertigo Games company website project Version 2

## Spec
```
- React.js
- Sass
```


## Build Setup
1. if you don't have ```yarn```, follow the Package Manager [yarn](https://yarnpkg.com/getting-started/install) and install

2. install dependencies```yarn install``` 
  
3. serve with hot reload at localhost:8080 ```yarn start``` 
  
4. build for production with minification ```yarn build``` 
  
5. check ```build``` folder.
  
6. confirmed [Vertigogames.com](vertigogames.com)

